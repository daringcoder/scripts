#!/usr/bin/env bash

#echo "Adding ppa for php..."
sudo add-apt-repository -y ppa:ondrej/php
#sh ./update.sh
sudo apt-get update -y
sudo apt-get upgrade -y
#echo "Enter the PHP version you want to install..."
#php_versions=( 7.4 8.0 )

#select php_version in "${php_versions[@]}"; do
#    echo "You have chosen $php_version"
#    break
#done

#echo "Installing PHP ${php_version}..."
sudo apt-get install -y php$1 php$1-fpm php$1-cli php$1-gd
sudo apt-get install -y php$1-common php$1-curl php$1-mbstring
sudo apt-get install -y php$1-zip php$1-xml php$1-imap
sudo apt-get install -y php$1-mysql php$1-dev php$1-intl php-pear
#echo "$(php -v)"

#echo "Installing Composer..."
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
#composer
